module AnswersHelper
  def correct_icon_test_case(test_case_result)
    return "fa fa-check" if test_case_result.correct
    "fa fa-close"
  end
  
  def correct_style_test_case(test_case_result)
    return "success" if test_case_result.correct
    "danger"
  end

  def correct_icon(answer)
    return "fa fa-check" if answer.correct?
    "fa fa-close"
  end

  def correct_style(boolean)
    return "success" if boolean == true
    "danger"
  end
end
