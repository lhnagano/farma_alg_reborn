class AnswerTester
  PATH_TO_SAVE = "tmp/"
  USER_OUTPUT_END_FILE_NAME = "user_output.txt"
  EXPECTED_OUTPUT_END_FILE_NAME = "expected_output.txt"

  def initialize(answer:, file_name:)
    @answer = answer
    @file_name = file_name
  end

  def test
    code_runner = CodeRunner.new(
      file_name: @file_name,
      extension: @answer.lang_extension,
      source_code: @answer.content
    )

    code_runner.compile

    @answer.question.test_cases.each.inject([]) do |results, test_case|
      result = code_runner.run(inputs: test_case, not_compile: true)
        
      results << {
        test_case: test_case,
        correct: not(code_runner.has_compilation_error) && check_test_case(result, test_case.output),
        output: result.gsub("\r", "")
      }
    end
  end

  private

    def check_test_case(user_output, expected_output)
      file_name = SecureRandom.hex
      user_output_file = PATH_TO_SAVE + file_name + USER_OUTPUT_END_FILE_NAME
      expected_output_file = PATH_TO_SAVE + file_name + EXPECTED_OUTPUT_END_FILE_NAME

      File.open(user_output_file, 'w') { |file| file.write(user_output) }
      File.open(expected_output_file, 'w') { |file| file.write(expected_output) }

      `diff -abBE #{user_output_file} #{expected_output_file}`
      Rails.logger.info ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      Rails.logger.info "diff -abBE #{user_output_file} #{expected_output_file}"
      Rails.logger.info $?.exitstatus
      result = $?.exitstatus == 0 ? true : false

      #File.delete(user_output_file)
      #File.delete(expected_output_file)

      result
    end
end
